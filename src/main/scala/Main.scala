import java.io.IOException
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.scene.Parent

import presenter.MainScenePresenter
import view.{MainScene, MainSceneObj}

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafxml.core.{FXMLLoader, NoDependencyResolver}

/**
  * Created by Vuko on 07.05.2016.
  */
object Main extends JFXApp
{
  //wczytanie widoku z pliku fxml
  val resource = getClass.getResource("/" + MainSceneObj.fxml)
  if (resource == null)
  {
    throw new IOException("Cannot load resource: " + MainSceneObj.fxml)
  }

  val loader: FXMLLoader = new FXMLLoader(resource, NoDependencyResolver)
  loader.load()
  val root = loader.getRoot[Parent]
  val view = loader.getController[MainScene]
  stage = new PrimaryStage
  {
    title = MainSceneObj.title
    scene = new Scene(root)
    //resizable = false
  }

  MainScenePresenter.init(view)

  //Przy zmianie wysokosci okna... chce aby panele w oknie rowniez zmienili swoj rozmiar i sie "dostosowaly"
  stage.getScene.heightProperty().addListener(new ChangeListener[Number]
  {
    override def changed(observable: ObservableValue[_ <: Number], oldValue: Number, newValue: Number): Unit =
    {
      MainScenePresenter.changeGridHeight(newValue.intValue())
    }
  })
}