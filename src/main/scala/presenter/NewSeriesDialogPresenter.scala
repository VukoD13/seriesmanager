package presenter

import model.Series
import org.joda.time.format.DateTimeFormat
import view.NewSeriesDialog

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by Vuko on 11.05.2016.
  */
object NewSeriesDialogPresenter
{
  var view: Option[NewSeriesDialog] = None

  def isAllDigits(x: String) = x forall Character.isDigit

  //string to date
  def parseDate(input: String) =
  {
    val fmt = DateTimeFormat forPattern "dd.MM.yyyy"
    try
    {
      Some(fmt.parseLocalDate(input))
    }
    catch
    {
      case e: IllegalArgumentException => None
    }
  }

  def applySeries(title: String, season: String, lastEpisode: String, lastDownloaded: String, dateOfNextEpisode: String): Future[Boolean] =
  {
    println("/////Serial/////")
    println(title)
    println(season)
    println(lastDownloaded)
    println(lastEpisode)
    println(dateOfNextEpisode)
    println("//////////")
    Future
    {
      if (validateSeriesForm(title, season, lastEpisode, lastDownloaded, dateOfNextEpisode))
      {
        val s: Series = new Series(title, season.toInt, lastEpisode.toInt, lastDownloaded.toInt, parseDate(dateOfNextEpisode).get)
        MainScenePresenter.addSeries(s)
        true
      }
      else
      {
        false
      }
    }
  }

  def validateSeriesForm(title: String, season: String, lastWatched: String, lastDownloaded: String, dateOfNextEpisode: String): Boolean =
  {
    (!title.isEmpty
      && !season.isEmpty && isAllDigits(season)
      && !lastWatched.isEmpty && isAllDigits(lastWatched)
      && !lastDownloaded.isEmpty && isAllDigits(lastDownloaded) && lastDownloaded >= lastWatched
      && parseDate(dateOfNextEpisode).isDefined
      )
  }
}