package presenter

import java.awt.Desktop
import java.io.IOException
import java.net.{URI, URL}
import java.text.{DecimalFormat, NumberFormat}
import javafx.fxml.FXMLLoader

import model.Series
import org.joda.time.LocalDate
import view._

import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.application.Platform

/**
  * Created by Vuko on 11.05.2016.
  */
object MainScenePresenter
{
  var view: Option[MainScene] = None

  //zmienia wysokosc panelu w oknie
  def changeGridHeight(windowHeight: Int): Unit =
  {
    view.get.changeGridHeight(windowHeight)
  }

  def setView(v: MainScene): Unit =
  {
    view = Some(v)
  }

  def init(v: MainScene): Unit =
  {
    setView(v)
    view.get.setRowHeight()

    Series.loadAll().onSuccess
    {
      case true =>
        sortSeries()
        Platform.runLater
        {
          view.get.buildGrid(getSeriesList)
        }
    }
  }

  def getSeriesList: List[Series] =
  {
    Series.get
  }

  def save(): Unit =
  {
    Series.saveAll()
  }

  //def load(u:() => Unit)

  def getNewSeriesDialogFxml: URL =
  {
    val resource = getClass.getResource("/" + NewSeriesDialogObj.fxml)
    if (resource == null)
    {
      throw new IOException("Cannot load resource: " + NewSeriesDialogObj.fxml)
    }
    else
    {
      NewSeriesDialogPresenter.view = Some(new FXMLLoader(resource).getController[NewSeriesDialog])
      resource
    }
  }

  def addSeriesToSeriesList(s: Series): Unit =
  {
    Series.get = s::Series.get //dodaj nowy serial na poczatek listy seriali
  }

  def addSeries(s: Series): Unit =
  {
    addSeriesToSeriesList(s)
    Platform.runLater
    {
      view.get.addToGrid(s, 1)
    }
  }

  def removeSeriesByTitle(title: String): Unit =
  {
    Series.get = Series.get.filter(_.title != title)
  }

  def watchSeriesEpisode(s: Series): Unit =
  {
    if(s.lastDownloaded > s.lastWatchedEpisode)
    {
      s.lastWatchedEpisode = s.lastWatchedEpisode + 1
    }
  }

  def downloadSeriesEpisode(s: Series): Unit =
  {
    s.lastDownloaded = s.lastDownloaded + 1
    changeDateForSeries(s, s.dateOfNextEpisode.plusWeeks(1))
  }

  def changeDateForSeries(s: Series, date: LocalDate): Unit =
  {
    s.dateOfNextEpisode = date
  }

  //tworze link do episodu danego serialu, przy pomocy ktorego moge go odszukac na danej stronie
  def getLink(s: Series): String =
  {
    val format: NumberFormat = new DecimalFormat("00")

    val base = "https://www.google.pl/#safe=off&q="
    val add = new StringBuilder(s.title)
      .append(" s")
      .append(format.format(s.season))
      .append("e")
      .append(format.format(s.lastDownloaded + 1))
      .toString().replace(" ", "+")
    base.concat(add)

//    //todo: :)
//    val base = "https://cos.cr/usearch/"
//    val add = new StringBuilder(s.title)
//      .append(" s")
//      //.append(String.format("%02d", s.season.toString))
//      .append(format.format(s.season))
//      .append("e")
//      .append(format.format(s.lastDownloaded + 1))
//      .toString().replace(" ", "%20")
//    base.concat(add)
  }

  //wyszukuje serial online uruchamiajac domyslna przegladarke
  def findSeriesOnline(s: Series): Unit =
  {
    if(Desktop.isDesktopSupported)
    {
      Desktop.getDesktop.browse(new URI(getLink(s)))
    }
  }

  def getSeriesByTitle(title: String): Series =
  {
    Series.get.filter(_.title == title).head
  }

  def sortSeries(): Unit =
  {
    Series.get = getSeriesList
      .sortBy(s => (-(s.lastDownloaded - s.lastWatchedEpisode), s.dateOfNextEpisode.toString, s.title))
  }
}