package model

import java.io._

import org.joda.time.LocalDate
import play.api.libs.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by Vuko on 08.05.2016.
  */
object Series
{
  val FILE: String = "serieslist.txt"
  implicit val seriesFormat = Json.format[Series]
  var get: List[Series] = Nil //lista seriali

  //funkcja parsujaca json do listy seriali
  def jsValueToListOfSeries(js: JsValue): List[Series] =
  {
    (js \ "series").as[List[Series]]
  }

  def loadAll(): Future[Boolean] =
  {
    println("Load")
    //ladowanie json z pliku...
    Future
    {
      val stream = new FileInputStream(FILE)
      val json =
        try
        {
          Json.parse(stream)
        }
        finally
        {
          stream.close()
        }
      get = jsValueToListOfSeries(json) //...by otrzymac i zwrocic liste seriali
      true
    }
  }

  //parsowanie listy seriali do obiektu json
  def getListOfSeriesAsJsonObject(series: List[Series]): JsObject =
  {
    Json.obj("series" -> series)
  }

  //zapisywanie listy seriali do pliku
  def saveAll() =
  {
    println("Save")
    val writer = new PrintWriter(new File(FILE))
    writer.write(getListOfSeriesAsJsonObject(get).toString())
    writer.close()
  }
}

case class Series(title: String,
                  season: Int = 1,
                  var lastWatchedEpisode: Int = 0,
                  var lastDownloaded: Int = 0,
                  var dateOfNextEpisode: LocalDate = LocalDate.now())
