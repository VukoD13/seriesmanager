package view

import java.time.LocalDate
import javafx.stage.Stage

import model.Series
import presenter.{MainScenePresenter, NewSeriesDialogPresenter}

import scala.language.postfixOps
import scalafx.Includes._
import scalafx.event.ActionEvent
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout.{GridPane, Priority, RowConstraints}
import scalafx.scene.text.{Font, FontWeight, Text}
import scalafx.stage.Modality
import scalafxml.core.macros.sfxml
import scalafxml.core.{FXMLView, NoDependencyResolver}

/**
  * Created by Vuko on 07.05.2016.
  */
trait MainScene
{
  def setRowHeight(): Unit
  def buildGrid(list: List[Series]): Unit
  def addToGrid(s: Series, row: Int)
  def changeGridHeight(windowWidth: Int): Unit
}

@sfxml
class MainSceneImpl(val grid: GridPane,
                    val sp: ScrollPane,
                    val newSeriesButton: Button,
                    val saveButton: Button) extends MainScene
{
  var rowHeight: Option[Double] = None
  val rowConstraints: RowConstraints = new RowConstraints

  override def setRowHeight(): Unit =
  {
    rowHeight = Option(grid.getRowConstraints.get(0).getPrefHeight) //biore pierwszy wiersz i pobieram wysokosc wiersza, dzieki czemu moge ja kontrolowac...
    //...i odpowiednio ustawic...
    val height: Double = rowHeight.getOrElse(50)
    rowConstraints.setMinHeight(height)
    rowConstraints.setPrefHeight(height)
    rowConstraints.setMaxHeight(height*1.3)
    rowConstraints.setVgrow(Priority.Sometimes)
  }

  //funkcja przekazuje liste seriali na widok - budowanie tabeli
  override def buildGrid(list: List[Series]): Unit =
  {
    println("build grid")
    var rowIndex = 1
    list.foreach(s =>
    {
      addToGrid(s, rowIndex)
      rowIndex = rowIndex + 1
    })
  }

  //zmienia wysokosc paneli w glownej scenie aplikacji
  override def changeGridHeight(windowHeight: Int): Unit =
  {
    val help = windowHeight - 150
    sp.setPrefHeight(help)
    grid.setPrefHeight(help)
  }

  //zwraca parametry wyskakujacego okienka, ktore pozwala na wprowadzenie do danych nowego serialu
  def newSeriesDialogStage(parentStage: Stage): Stage =
  {
    val dialogStage: Stage = new Stage()
    dialogStage.setTitle(NewSeriesDialogObj.title)
    dialogStage.initModality(Modality.WindowModal)
    dialogStage.initOwner(parentStage)
    dialogStage.setResizable(false)
    dialogStage
  }

  saveButton.onAction = (event: ActionEvent) =>
  {
    MainScenePresenter.save()
  }

  //wywolanie formularza, na ktorym mozna wprowadzic dane nowego serialu
  newSeriesButton.onAction = (event: ActionEvent) =>
  {
    val stage: Stage = grid.getScene.getWindow.asInstanceOf[Stage]
    val resource = MainScenePresenter.getNewSeriesDialogFxml
    val dialogStage: Stage = newSeriesDialogStage(stage)
    val scene = new Scene(FXMLView(resource, NoDependencyResolver))
    dialogStage.setScene(scene)
    dialogStage.showAndWait()
  }

  override def addToGrid(s: Series, rowPos: Int): Unit =
  {
    grid.getRowConstraints.add(rowConstraints) //dodaje nowy wiersz w tabeli

    Row.getAll.filter(_.pos >= rowPos).foreach(r => //filtuje wiersze zostawiajac te bedace dalej w tabeli, nastepnie iteruje...
    {
      r.setPos(r.pos + 1) //...by je odpowiednio przestawic i zaktualizowac widok
    })

    //tworze node'y
    val title: Text = new Text(s.title)
    val season: Text = new Text(s.season.toString)
    val watched: Text = new Text(s.lastWatchedEpisode.toString)
    val downloaded: Text = new Text(s.lastDownloaded.toString)
    val nextDate: Text = new Text(s.dateOfNextEpisode.toString)

    val row: Row = new Row(rowPos, title, season, watched, downloaded, nextDate)
    Row.addToRowList(row)
    row.addToGrid(grid)
  }
}

object MainSceneObj
{
  val title: String = "Series Manager"
  val fxml: String = "main_scene.fxml"
}

case class Result(localDate: String)
//klasa odwzorujaca wiersz w tabeli
class Row(var pos: Int,
          title: Text,
          season: Text,
          lastWatched: Text,
          lastDownloaded: Text,
          dateOfNextEpisode: Text)
{
  val series: Series = MainScenePresenter.getSeriesByTitle(title.text.value)

  val row: Text = new Text(pos.toString)
  val watchSeriesButton: Button = new Button(Row.watchButtonText)
  val downloadSeriesButton: Button = new Button(Row.downloadButtonText)
  val getOnlineButton: Button = new Button(Row.getOnlineButtonText)
  val setBreakButton: Button = new Button(Row.setBreakButtonText)
  val deleteSeriesButton: Button = new Button(Row.deleteButtonText)

  val nodes = Array(row, title, season, lastWatched, lastDownloaded, dateOfNextEpisode, watchSeriesButton, downloadSeriesButton, getOnlineButton, setBreakButton, deleteSeriesButton)

  nodes.foreach(x =>
  {
    x.focusTraversable = false //nie chce aby jakis node byl domyslnie zaznaczony
    //noinspection TypeCheckCanBeMatch
    if(x.isInstanceOf[Text]) //dla nodow bedacych tekstem -> taki label z wieksza mozliwoscia customizacji
    {
      x.asInstanceOf[Text].setFont(Font.font("System", 15)) //ustawiam wielkosc czcionki
    }
  })
  checkTextsAndSetBold()

  //przekazuje nody wiersza na panel widoku
  def addToGrid(grid: GridPane): Unit =
  {
    for(i <- nodes.indices)
    {
      grid.add(nodes(i), i, pos)
    }

    watchSeriesButton.onAction = (event: ActionEvent) =>
    {
      MainScenePresenter.watchSeriesEpisode(series)
      lastWatched.text.value = series.lastWatchedEpisode.toString
      checkTextsAndSetBold()
    }

    downloadSeriesButton.onAction = (event: ActionEvent) =>
    {
      MainScenePresenter.downloadSeriesEpisode(series)
      lastDownloaded.text.value = series.lastDownloaded.toString
      dateOfNextEpisode.text.value = series.dateOfNextEpisode.toString
      checkTextsAndSetBold()
    }

    getOnlineButton.onAction = (event: ActionEvent) =>
    {
      MainScenePresenter.findSeriesOnline(series)
    }

    //stworzenie i obsluga okna, ktore umozliwia uzytkownikowi zmiane daty kolejnego odcinka serialu
    //region breakButtonAction
    setBreakButton.onAction = (event: ActionEvent) =>
    {
      val stage: Stage = grid.getScene.getWindow.asInstanceOf[Stage]
      val dialog = new Dialog[Result]()
      {
        initOwner(stage)
        title = "Zmień datę"
        headerText = series.title
      }

      dialog.dialogPane().buttonTypes = Seq(ButtonType.Apply, ButtonType.Cancel)

      val date: DatePicker = new DatePicker()
      date.value.value = LocalDate.parse(series.dateOfNextEpisode.toString)

      val g = new GridPane()
      {
        hgap = 10
        vgap = 10
        padding = Insets(20, 100, 10, 10)

        add(date, 1, 0)
      }

      dialog.dialogPane().content = g

      dialog.resultConverter = dialogButton =>
        if(dialogButton.getButtonData.toString == "APPLY")
          Result(date.getEditor.getText())
        else
          null

      val result = dialog.showAndWait()

      result match
      {
        case Some(maybeLocalDate: Result) =>
          NewSeriesDialogPresenter.parseDate(maybeLocalDate.localDate) match
          {
            case Some(localDate) =>
              println("zmiana daty na " + localDate.toString)
              MainScenePresenter.changeDateForSeries(series, localDate)
              dateOfNextEpisode.text.value = localDate.toString
              checkTextsAndSetBold()
            case None =>
              println("Zla ta data...")
          }
        case None       => println("Dialog was canceled.")
      }
    }
    //endregion

    //ustawiam akcje przycisku usuwajacego wiersz
    deleteSeriesButton.onAction = (event: ActionEvent) =>
    {
      removeFromGrid(grid) //usuwam obiekty wiersza z widoku...
      MainScenePresenter.removeSeriesByTitle(title.text.value) //... oraz z modelu
      Row.getAll.filter(_.pos >= pos).foreach(r => //aktualizuje pozycje obiektow dalszych wierszy
      {
        r.setPos(r.pos - 1)
      })
      grid.getRowConstraints.remove(pos) //usuwam ostatecznie dany wiersz
    }
  }

  //funkcja usuwa z panelu widoku nody wiersza
  def removeFromGrid(grid: GridPane): Unit =
  {
    for(i <- nodes.indices)
    {
      grid.children.remove(nodes(i))
    }
  }

  //funkcja zmienia pozycje wiersza, po czym aktualizuje odpowiednio pozycje jego obiektow
  def setPos(num: Int): Unit =
  {
    pos = num
    row.text.value = pos.toString

    for(i <- nodes.indices)
    {
      GridPane.setRowIndex(nodes(i), pos)
    }
  }

  def checkTextsAndSetBold(): Unit =
  {
    if(series.lastDownloaded > series.lastWatchedEpisode)
    {
      lastDownloaded.setFont(Font.font(lastDownloaded.getFont.getFamily, FontWeight.Bold, lastDownloaded.getFont.getSize))
    }
    else
    {
      lastDownloaded.setFont(Font.font(lastDownloaded.getFont.getFamily, FontWeight.Normal, lastDownloaded.getFont.getSize))
    }

    if(series.dateOfNextEpisode.isBefore(org.joda.time.LocalDate.now().plusDays(1)))
    {
      dateOfNextEpisode.setFont(Font.font(dateOfNextEpisode.getFont.getFamily, FontWeight.Bold, dateOfNextEpisode.getFont.getSize))
    }
    else
    {
      dateOfNextEpisode.setFont(Font.font(dateOfNextEpisode.getFont.getFamily, FontWeight.Normal, dateOfNextEpisode.getFont.getSize))
    }
  }
}

object Row
{
  var getAll: List[Row] = Nil

  val watchButtonText = "Obejrzany"
  val downloadButtonText = "Pobrany"
  val getOnlineButtonText = "Online"
  val setBreakButtonText = "Przerwa"
  val deleteButtonText = "Usun"

  def addToRowList(row: Row): Unit = //opcjonalna funkcja, kod jest czytelniejszy
  {
    getAll = getAll.::(row)
  }
}