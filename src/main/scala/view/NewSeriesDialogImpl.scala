package view

import java.time.LocalDate
import javafx.stage.Stage

import presenter.NewSeriesDialogPresenter

import scala.concurrent.ExecutionContext.Implicits.global
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.scene.control.{ButtonType, DatePicker, DialogPane, TextField}
import scalafxml.core.macros.sfxml

/**
  * Created by Vuko on 11.05.2016.
  */
trait NewSeriesDialog
{
}

@sfxml
class NewSeriesDialogImpl(val dialog: DialogPane,
                          val titleText: TextField,
                          val seasonText: TextField,
                          val lastEpisodeText: TextField,
                          val dateOfNextEpisodePicker: DatePicker) extends  NewSeriesDialog
{
  dateOfNextEpisodePicker.setValue(LocalDate.now.plusWeeks(1)) //ustawienie poczatkowej daty na tydzien od teraz
  lastEpisodeText.text.set("0") //ustawienie domyslnej wartosi dla ostatnio obejrzanego odcinka - 0 -> zazwyczaj uzytkownik bedzie dodawal nowy sezon/serial, a nie juz aktualnie ogladany

  //przycisk Cancel - zamkniecie okna
  dialog.lookupButton(ButtonType.Cancel).onMouseClicked = handle
  {
    val stage: Stage = dialog.getScene.getWindow.asInstanceOf[Stage]
    stage.close()
  }

  //przycisk Apply
  dialog.lookupButton(ButtonType.Apply).onMouseClicked = handle
  {
    val stage: Stage = dialog.getScene.getWindow.asInstanceOf[Stage]

    //pobranie zmiennych z formularza
    val title = textOf(titleText)
    val season = textOf(seasonText)
    val lastEpisode = textOf(lastEpisodeText)
    val dateOfNextEpisode = dateOfNextEpisodePicker.getEditor.getText()

    NewSeriesDialogPresenter.applySeries(title, season, lastEpisode, lastEpisode, dateOfNextEpisode).onSuccess
    {
      case true =>
        println("dane serialu poprawne")
        Platform.runLater
        {
          stage.close()
        }
      case false =>
        println("dane serialu niepoprawne...")
    }
  }

  def textOf(t: TextField): String =
  {
    t.text.value
  }
}

object NewSeriesDialogObj
{
  val title: String = "Nowy serial/sezon"
  val fxml: String = "new_series_dialog.fxml"
}