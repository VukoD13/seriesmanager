import javafx.application.Application
import javafx.stage.Stage

import scalafx.application.JFXApp

object FXStart extends Application
{
  def main(args: Array[String]) =
  {
    Main.main(Array())
  }

  def start(primaryStage: Stage): Unit =
  {
    println("START")
    JFXApp.Stage = primaryStage
    //JFXApp.ActiveApp.delayedInit(5)
    if (JFXApp.AutoShow)
    {
      JFXApp.Stage.show()
    }
  }

  override def stop()
  {
    JFXApp.ActiveApp.stopApp()
  }
}