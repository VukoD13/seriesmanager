import presenter.NewSeriesDialogPresenter

/**
  * Created by Vuko on 12.05.2016.
  */
class NewSeriesDialogImplPresenterTest extends TestBase
{
  "isAllDigits" should "check if given string contains only digits" in
  {
    var str: String = ""
    assert(NewSeriesDialogPresenter.isAllDigits(str) === true)

    str = "13245"
    assert(NewSeriesDialogPresenter.isAllDigits(str) === true)

    str = "13.245"
    assert(NewSeriesDialogPresenter.isAllDigits(str) === false)

    str = "a"
    assert(NewSeriesDialogPresenter.isAllDigits(str) === false)

    str = "#@!213"
    assert(NewSeriesDialogPresenter.isAllDigits(str) === false)
  }

  "parseDate" should "return Some(LocalDate) if given string was date in proper format" in
  {
    var str: String = ""
    assert(NewSeriesDialogPresenter.parseDate(str) === None)

    str = "asdd"
    assert(NewSeriesDialogPresenter.parseDate(str) === None)

    str = "32.10.1994"
    assert(NewSeriesDialogPresenter.parseDate(str) === None)

    str = "25.13.1994"
    assert(NewSeriesDialogPresenter.parseDate(str) === None)

    str = "12.5.2016"
    assert(NewSeriesDialogPresenter.parseDate(str) !== None)

    str = "12.05.2016"
    assert(NewSeriesDialogPresenter.parseDate(str).get.year().get() === 2016)
    assert(NewSeriesDialogPresenter.parseDate(str).get.monthOfYear().get() === 5)
    assert(NewSeriesDialogPresenter.parseDate(str).get.dayOfMonth().get() === 12)
  }

  "validateSeriesForm" should "return true if all given data is correct" in
  {
    assert (
      NewSeriesDialogPresenter.validateSeriesForm(title = "", season = "", lastWatched = "", lastDownloaded = "", dateOfNextEpisode = "")
      === false
    )

    assert (
      NewSeriesDialogPresenter.validateSeriesForm(title = "Gotham", season = "2", lastWatched = "20", lastDownloaded = "21", dateOfNextEpisode = "17.05.2016")
        === true
    )
  }
}
