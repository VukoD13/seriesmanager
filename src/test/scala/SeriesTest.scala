import model.Series
import org.joda.time.LocalDate
import play.api.libs.json.{JsObject, Json}

/**
  * Created by Vuko on 08.05.2016.
  */
class SeriesTest extends TestBase
{
  "jsValueToListOfSeries" should "return valid list of series from JsValue" in
  {
    val actual: List[Series] = Series.jsValueToListOfSeries(Json.parse("{\"series\":[{\"title\":\"xd\",\"season\":1,\"lastWatchedEpisode\":0,\"lastDownloaded\":0,\"dateOfNextEpisode\":\"2016-05-16\"},{\"title\":\"Gotham\",\"season\":2,\"lastWatchedEpisode\":15,\"lastDownloaded\":15,\"dateOfNextEpisode\":\"2016-05-21\"}]}"))
    val expected: List[Series] = List(new Series("xd", dateOfNextEpisode = new LocalDate(2016, 5, 16)), new Series("Gotham", season = 2, lastWatchedEpisode = 15, lastDownloaded = 15, dateOfNextEpisode = new LocalDate(2016, 5, 21)))

    assert(actual === expected)
    assert(actual(1).title === expected(1).title)
  }

  "getListOfSeriesAsJsonObject" should "return valid JsonObject from list of series" in
    {
      val actual: JsObject = Json.parse("{\"series\":[{\"title\":\"xd\",\"season\":1,\"lastWatchedEpisode\":0,\"lastDownloaded\":0,\"dateOfNextEpisode\":\"2016-05-16\"},{\"title\":\"Gotham\",\"season\":2,\"lastWatchedEpisode\":15,\"lastDownloaded\":15,\"dateOfNextEpisode\":\"2016-05-21\"}]}").as[JsObject]
      val expected: JsObject = Series.getListOfSeriesAsJsonObject(List(new Series("xd", dateOfNextEpisode = new LocalDate(2016, 5, 16)), new Series("Gotham", season = 2, lastWatchedEpisode = 15, lastDownloaded = 15, dateOfNextEpisode = new LocalDate(2016, 5, 21))))

      assert(actual === expected)
    }
}