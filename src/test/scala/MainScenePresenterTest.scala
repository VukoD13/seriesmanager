import model.Series
import org.joda.time.LocalDate
import presenter.MainScenePresenter

import scala.util.Random

/**
  * Created by Vuko on 16.05.2016.
  */
class MainScenePresenterTest extends TestBase
{
  "addSeriesToSeriesList" should "add series at the beginning of the series list" in
  {
    MainScenePresenter.addSeriesToSeriesList(new Series("lol"))
    var actual = MainScenePresenter.getSeriesList.size
    assert(actual === 1)

    val title = "nowy"
    MainScenePresenter.addSeriesToSeriesList(new Series(title))
    actual = MainScenePresenter.getSeriesList.size
    assert(actual === 2)
    assert(MainScenePresenter.getSeriesList.head.title === title)

    Series.get = List()
  }

  "removeSeriesByTitle" should "remove series from the series list using given title" in
  {
    val (title1, title2, title3) = ("a", "b", "c")
    Series.get = List(new Series(title1), new Series(title2), new Series(title3))
    assert(MainScenePresenter.getSeriesList.size === 3)

    val newSize = 2
    MainScenePresenter.removeSeriesByTitle(title2)
    assert(MainScenePresenter.getSeriesList.size === newSize)
    val beSure = MainScenePresenter.getSeriesList.filter(_.title != title2) //upewniam sie ze usunieto poprawny serial
    assert(beSure.size === newSize)

    Series.get = List()
  }

  "watchSeriesEpisode" should "add 1 to watched episodes number if episode is downloaded(bigger number)" in
  {
    val s: Series = new Series("xd")
    assert(s.lastWatchedEpisode === 0 && s.lastDownloaded === 0)
    MainScenePresenter.watchSeriesEpisode(s)
    assert(s.lastWatchedEpisode === 0 && s.lastDownloaded === 0)

    s.lastDownloaded = 1
    MainScenePresenter.watchSeriesEpisode(s)
    assert(s.lastWatchedEpisode === 1 && s.lastDownloaded === 1)
    MainScenePresenter.watchSeriesEpisode(s)
    assert(s.lastWatchedEpisode === 1 && s.lastDownloaded === 1)
  }

  "downloadSeriesEpisode" should "add 1 to downloaded episodes number and add 1 week to dateOfNextEpisode" in
  {
    val ld = new LocalDate(2016, 1, 1)
    val s: Series = new Series("abc", dateOfNextEpisode = ld)
    assert(s.lastDownloaded === 0)
    MainScenePresenter.downloadSeriesEpisode(s)
    assert(s.lastDownloaded === 1 && s.dateOfNextEpisode === ld.plusWeeks(1))
    MainScenePresenter.downloadSeriesEpisode(s)
    assert(s.lastDownloaded === 2 && s.dateOfNextEpisode === new LocalDate(2016, 1, 15))
  }

  "getSeriesByTitle" should "return correct series" in
  {
    val (title1, title2, title3) = ("a", "b", "c")
    Series.get = List(new Series(title1), new Series(title2), new Series(title3))
    val s: Series = MainScenePresenter.getSeriesByTitle(title2)
    assert(s === Series.get(1) && s.title === title2)

    Series.get = List()
  }

  "getLink" should "create correct link for series" in
  {
    val expectedLink: String = "https://www.google.pl/#safe=off&q=Gotham+s02e16"
    val s: Series = new Series("Gotham", season = 2, lastDownloaded =  15)
    val link: String = MainScenePresenter.getLink(s)

    assert(link === expectedLink)
  }

  "changeDateForSeries" should "change date for given series" in
  {
    val s: Series = new Series("xd", dateOfNextEpisode = new LocalDate(2016, 1, 1))
    MainScenePresenter.changeDateForSeries(s, new LocalDate(2016, 2, 2))
    assert(s.dateOfNextEpisode === new LocalDate(2016, 2, 2))
  }

  "sortSeries" should "sort series by size of downloaded - watched episodes, date and name" in
  {
    val a = new Series("za", lastDownloaded = 5, lastWatchedEpisode = 2, dateOfNextEpisode = new LocalDate(2016, 1, 1))  //2
    val b = new Series("zz", lastDownloaded = 5, lastWatchedEpisode = 1, dateOfNextEpisode = new LocalDate(2016, 1, 1))  //1
    val c = new Series("zz", lastDownloaded = 5, lastWatchedEpisode = 2, dateOfNextEpisode = new LocalDate(2016, 2, 2))  //4
    val d = new Series("zc", lastDownloaded = 5, lastWatchedEpisode = 2, dateOfNextEpisode = new LocalDate(2016, 2, 2))  //3
    val e = new Series("xd", lastDownloaded = 0, lastWatchedEpisode = 0, dateOfNextEpisode = new LocalDate(2016, 1, 2))  //5

    for(i <- 0 to 10)
    {
      Series.get = List(a, b, c, d, e)
      Series.get = Random.shuffle(Series.get)

      MainScenePresenter.sortSeries()

      //noinspection ZeroIndexToHead
      assert(Series.get(0) === b && b.title === "zz")
      assert(Series.get(1) === a)
      assert(Series.get(2) === d && d.title === "zc")
      assert(Series.get(3) === c)
      assert(Series.get(4) === e)
    }
    Series.get = List()
  }
}
