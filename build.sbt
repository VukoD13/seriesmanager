name := "SeriesManager"

version := "1.0"

scalaVersion := "2.11.8"

mainClass in Compile := Some("Ops")

//resolvers += Resolver.sonatypeRepo("releases")
addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

// Add dependency on ScalaFX library
libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.92-R10"
//FXML
libraryDependencies += "org.scalafx" %% "scalafxml-core-sfx8" % "0.2.2"
//Play Json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.3"
//ScalaTest
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
// Fork a new JVM for 'run' and 'test:run', to avoid JavaFX double initialization problems
fork := true